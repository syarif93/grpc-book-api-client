package main

import (
	"context"
	"strconv"

	"client/helpers"
	"client/helpers/validators"
	"client/models"
	"client/proto/pb/book"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Post("/book", func(c *fiber.Ctx) error {
		reqBody := models.Book{}
		if errBody := c.BodyParser(&reqBody); errBody != nil {
			return c.Status(422).JSON(map[string]any{
				"message": "Request body is empty",
				"error":   errBody.Error(),
			})
		}
		if errValidation := validators.ValidateBook(&reqBody); errValidation != nil {
			return c.Status(422).JSON(map[string]any{
				"message": "Validation error",
				"error":   errValidation,
			})
		}

		conn, err := helpers.GRPCConn()
		if err != nil {
			conn.Close()
			return c.Status(500).JSON(err)
		}

		client := book.NewBookServiceClient(conn)
		req := book.CreateBookRequest{
			Book: &book.Book{
				Title:     reqBody.Title,
				Author:    reqBody.Author,
				PageCount: reqBody.PageCount,
				Language:  reqBody.Language,
			},
		}

		res, errBook := client.CreateBook(context.Background(), &req)
		if errBook != nil {
			return c.Status(500).JSON(errBook.Error())
		}

		return c.Status(200).JSON(res.Book)
	})

	app.Get("/book/:bookID", func(c *fiber.Ctx) error {
		bookId := c.Params("bookId")
		intBookId, _ := strconv.Atoi(bookId)

		conn, err := helpers.GRPCConn()
		if err != nil {
			conn.Close()
			return c.Status(500).JSON(err)
		}

		client := book.NewBookServiceClient(conn)
		req := book.GetBookByIDRequest{BookId: int32(intBookId)}

		res, errBook := client.GetBookByID(context.Background(), &req)
		if errBook != nil {
			return c.Status(500).JSON(errBook.Error())
		}

		return c.Status(200).JSON(res.Book)
	})

	app.Patch("/book/:bookID", func(c *fiber.Ctx) error {
		reqBody := models.Book{}
		if errBody := c.BodyParser(&reqBody); errBody != nil {
			return c.Status(422).JSON(map[string]any{
				"message": "Request body is empty",
				"error":   errBody.Error(),
			})
		}
		if errValidation := validators.ValidateBook(&reqBody); errValidation != nil {
			return c.Status(422).JSON(map[string]any{
				"message": "Validation error",
				"error":   errValidation,
			})
		}

		bookId := c.Params("bookId")
		intBookId, _ := strconv.Atoi(bookId)

		conn, err := helpers.GRPCConn()
		if err != nil {
			conn.Close()
			return c.Status(500).JSON(err)
		}

		client := book.NewBookServiceClient(conn)
		req := book.UpdateBookByIDRequest{BookId: int32(intBookId), Book: &book.Book{
			Title:     reqBody.Title,
			Author:    reqBody.Author,
			PageCount: reqBody.PageCount,
			Language:  reqBody.Language,
		}}

		res, errBook := client.UpdateBookByID(context.Background(), &req)
		if errBook != nil {
			return c.Status(500).JSON(errBook.Error())
		}

		return c.Status(200).JSON(res.Book)
	})

	app.Delete("/book/:bookID", func(c *fiber.Ctx) error {
		bookId := c.Params("bookId")
		intBookId, _ := strconv.Atoi(bookId)

		conn, err := helpers.GRPCConn()
		if err != nil {
			conn.Close()
			return c.Status(500).JSON(err)
		}

		client := book.NewBookServiceClient(conn)
		req := book.DeleteBookByIDRequest{BookId: int32(intBookId)}

		res, errBook := client.DeleteBookByID(context.Background(), &req)
		if errBook != nil {
			return c.Status(500).JSON(errBook.Error())
		}

		return c.Status(200).JSON(res.Book)
	})

	app.Get("/books", func(c *fiber.Ctx) error {
		conn, err := helpers.GRPCConn()
		if err != nil {
			conn.Close()
			return c.Status(500).JSON(err)
		}

		client := book.NewBookServiceClient(conn)
		req := book.GetBookListRequest{}

		res, errBook := client.GetBookList(context.Background(), &req)
		if errBook != nil {
			return c.Status(500).JSON(errBook.Error())
		}

		return c.Status(200).JSON(res.Books)
	})

	app.Listen(":8000")
}
