package models

import "time"

type Book struct {
	ID        int32
	Title     string `json:"title" validate:"required"`
	Author    string `json:"author" validate:"required"`
	PageCount int32  `json:"page_count" validate:"required"`
	Language  string `json:"language" validate:"required"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
