package helpers

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func GRPCConn() (*grpc.ClientConn, error) {
	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	return conn, nil
}
