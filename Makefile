dev:
	nodemon --exec go run main.go --signal SIGTERM

proto.compile:
	protoc --proto_path=proto proto/*.proto --go_out=./proto --go-grpc_out=./proto